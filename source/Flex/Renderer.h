#pragma once

#include <dxgi.h>
#include <d3dcommon.h>
#include <d3d11.h>
#include <DirectXMath.h>

#include "Align.h"
#include "Utilities.h"

#include "Camera.h"
#include "IndexedPrimitive.h"
#include "ColorShader.h"

#include "TexturedVertex.h"
#include "TextureShader.h"
#include "Texture2D.h"

#include "TexturedNormalVertex.h"
#include "DiffuseShader.h"

#include "Sprite.h"

namespace Flex
{
	struct RendererSetup
	{
		HWND WindowHandle;
		unsigned int ScreenWidth;
		unsigned int ScreenHeight;
		bool IsWindowed;
	};

	class Renderer : public Align<16>
	{
	public:
		Renderer();

		bool Initialize(const RendererSetup setup);

		void Uninitialize();

		void BeginScene(float red, float green, float blue, float alpha);

		void Render();

		void EndScene();
	private:
		IDXGISwapChain* _swapChain;
		ID3D11Device* _device;
		ID3D11DeviceContext* _deviceContext;
		ID3D11RenderTargetView* _renderTargetView;
		ID3D11Texture2D* _depthStencilBuffer;
		ID3D11DepthStencilState* _3dDepthStencilState;
		ID3D11DepthStencilState* _2dDepthStencilState;
		ID3D11DepthStencilView* _depthStencilView;
		ID3D11RasterizerState* _rasterizerState;
		DirectX::XMMATRIX _projectionMatrix;
		DirectX::XMMATRIX _worldMatrix;
		DirectX::XMMATRIX _orthoMatrix;

		Camera* _camera;
		IndexedPrimitive<ColoredVertex>* _primitive1;
		IndexedPrimitive<TexturedVertex>* _primitive2;
		IndexedPrimitive<TexturedNormalVertex>* _primitive3;
		ColorShader* _colorShader;
		TextureShader* _textureShader;
		DiffuseShader* _diffuseShader;
		Texture2D* _texture;

		Sprite* _sprite1;
	};
}