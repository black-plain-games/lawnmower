#include "PrimitiveBase.h"

#include <exception>

#include "Utilities.h"

using namespace Flex;

PrimitiveBase::PrimitiveBase()
{
	_vertexBuffer = nullptr;
	_indexBuffer = nullptr;

	_vertexStride = 0;
	_vertexCount = 0;
	_indexCount = 0;
}

void PrimitiveBase::Initialize(
	ID3D11Device* device,
	unsigned long vertexCount,
	unsigned int vertexSize,
	void* vertices,
	unsigned long indexCount,
	unsigned long* indices)
{
	_vertexStride = vertexSize;

	_vertexCount = vertexCount;

	D3D11_BUFFER_DESC vertexBufferDescription;
	ZeroMemory(&vertexBufferDescription, sizeof(vertexBufferDescription));

	vertexBufferDescription.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDescription.ByteWidth = _vertexStride * _vertexCount;
	vertexBufferDescription.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDescription.CPUAccessFlags = 0;
	vertexBufferDescription.MiscFlags = 0;
	vertexBufferDescription.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA vertexData;
	ZeroMemory(&vertexData, sizeof(vertexData));

	vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	auto result = device->CreateBuffer(&vertexBufferDescription, &vertexData, &_vertexBuffer);

	if (FAILED(result))
	{
		throw std::exception("Failed to create vertex buffer");
	}

	_indexCount = indexCount;

	D3D11_BUFFER_DESC indexBufferDescription;
	ZeroMemory(&indexBufferDescription, sizeof(indexBufferDescription));

	indexBufferDescription.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDescription.ByteWidth = sizeof(unsigned long) * _indexCount;
	indexBufferDescription.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDescription.CPUAccessFlags = 0;
	indexBufferDescription.MiscFlags = 0;
	indexBufferDescription.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA indexData;
	ZeroMemory(&indexData, sizeof(indexData));

	indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	result = device->CreateBuffer(&indexBufferDescription, &indexData, &_indexBuffer);

	if (FAILED(result))
	{
		throw std::exception("Failed to create index buffer");
	}
}

void PrimitiveBase::Uninitialize()
{
	SafeRelease(_indexBuffer);
	SafeRelease(_vertexBuffer);
}

void PrimitiveBase::Render(ID3D11DeviceContext* deviceContext)
{
	unsigned int stride = _vertexStride;
	unsigned int offset = 0;

	deviceContext->IASetVertexBuffers(0, 1, &_vertexBuffer, &stride, &offset);
	deviceContext->IASetIndexBuffer(_indexBuffer, DXGI_FORMAT_R32_UINT, 0);
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	deviceContext->DrawIndexed(_indexCount, 0, 0);
}
