cbuffer MatrixBuffer
{
	matrix World;
	matrix View;
	matrix Projection;
};

struct VertexInputType
{
	float4 Position : POSITION;
	float2 TextureCoordinate : TEXCOORD0;
};

struct PixelInputType
{
	float4 Position : SV_POSITION;
	float2 TextureCoordinate : TEXCOORD0;
};

PixelInputType main(VertexInputType input)
{
	PixelInputType output;

	input.Position.w = 1.0f;

	output.Position = mul(input.Position, World);
	output.Position = mul(output.Position, View);
	output.Position = mul(output.Position, Projection);

	output.Position.z = 0.0f;

	output.TextureCoordinate = input.TextureCoordinate;

	return output;
}