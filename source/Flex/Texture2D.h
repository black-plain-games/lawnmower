#pragma once
#include <d3d11.h>

namespace Flex
{
	class Texture2D
	{
	public:
		Texture2D();
		~Texture2D();

		void Initialize(ID3D11Device*, const WCHAR*);
		void Uninitialize();

		inline ID3D11ShaderResourceView* GetTexture() { return _texture; }

	private:
		ID3D11ShaderResourceView* _texture;
	};
}