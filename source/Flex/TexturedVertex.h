#pragma once

#include <DirectXMath.h>

namespace Flex
{
	struct TexturedVertex
	{
		DirectX::XMFLOAT3 Position;
		DirectX::XMFLOAT2 TextureCoordinate;
	};
}