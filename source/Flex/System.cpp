#include "Engine.h"
#include "Renderer.h"

using namespace Flex;

LRESULT WINAPI ProcessMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	}

	return DefWindowProc(hWnd, msg, wParam, lParam);
}

Engine::Engine(LifecycleManager* lifecycleManager)
{
	_lifecycleManager = lifecycleManager;
}

int Engine::Run(HINSTANCE instanceHandle, EngineSetup setup)
{
	WNDCLASSEX wc =
	{
		sizeof(WNDCLASSEX),
		CS_CLASSDC,
		ProcessMessage,
		0L,
		0L,
		GetModuleHandle(nullptr),
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		setup.WindowName,
		nullptr
	};

	RegisterClassEx(&wc);

	HWND hWnd = CreateWindow(setup.WindowName,
		setup.WindowCaption,
		WS_OVERLAPPED,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		setup.ScreenWidth,
		setup.ScreenHeight,
		nullptr,
		nullptr,
		wc.hInstance,
		nullptr);

	ShowWindow(hWnd, SW_SHOWDEFAULT);

	UpdateWindow(hWnd);

	MSG msg;

	ZeroMemory(&msg, sizeof(msg));

	_lifecycleManager->PreInitialize();

	auto renderer = new Renderer();

	RendererSetup rendererSetup;

	rendererSetup.IsWindowed = setup.IsWindowed;
	rendererSetup.ScreenHeight = setup.ScreenHeight;
	rendererSetup.ScreenWidth = setup.ScreenWidth;
	rendererSetup.WindowHandle = hWnd;

	if (!renderer->Initialize(rendererSetup))
	{
		MessageBox(hWnd, "Renderer initialization failed", "Error", MB_OK);

		return 1;
	}

	_lifecycleManager->PostInitialize();

	while (msg.message != WM_QUIT)
	{
		if (PeekMessage(&msg, nullptr, 0U, 0U, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			_lifecycleManager->PreUpdate();

			// do update stuff here

			_lifecycleManager->PostUpdate();

			_lifecycleManager->PreRender();

			renderer->BeginScene(0, 0, 0, 1);

			renderer->Render();

			renderer->EndScene();

			_lifecycleManager->PostRender();
		}
	}

	_lifecycleManager->PreUnititialize();

	renderer->Uninitialize();

	delete renderer;

	UnregisterClass(setup.WindowName, wc.hInstance);

	return _lifecycleManager->PostUninitialize();
}