#pragma once

#include <d3d11.h>

#include "PrimitiveBase.h"

namespace Flex
{
	template <typename TVertex>
	class IndexedPrimitive : public PrimitiveBase
	{
	public:
		void Initialize(
			ID3D11Device* device,
			unsigned long vertexCount,
			TVertex* vertices,
			unsigned long indexCount,
			unsigned long* indices)
		{
			PrimitiveBase::Initialize(device, vertexCount, sizeof(TVertex), vertices, indexCount, indices);
		}
	};
}