#pragma once

namespace Flex
{
	template <unsigned char Alignment>
	class Align
	{
	public:
		void* operator new (size_t size)
		{
			return _aligned_malloc(size, Alignment);
		}

		void operator delete(void* p)
		{
			_aligned_free(p);
		}
	};
}