#include "Renderer.h"

using namespace Flex;

Renderer::Renderer()
{
	_swapChain = nullptr;
	_device = nullptr;
	_deviceContext = nullptr;
	_renderTargetView = nullptr;
	_depthStencilBuffer = nullptr;
	_3dDepthStencilState = nullptr;
	_2dDepthStencilState = nullptr;
	_depthStencilView = nullptr;
	_rasterizerState = nullptr;

	_projectionMatrix = DirectX::XMMatrixIdentity();
	_worldMatrix = DirectX::XMMatrixIdentity();
	_orthoMatrix = DirectX::XMMatrixIdentity();

	_camera = nullptr;
	_primitive1 = nullptr;
	_primitive2 = nullptr;
	_primitive3 = nullptr;
	_colorShader = nullptr;
	_textureShader = nullptr;
	_diffuseShader = nullptr;
	_texture = nullptr;
	_sprite1 = nullptr;
}

D3D11_DEPTH_STENCIL_DESC GetDefaultDepthStencilDescription()
{
	D3D11_DEPTH_STENCIL_DESC description;

	ZeroMemory(&description, sizeof(description));

	description.DepthEnable = true;
	description.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	description.DepthFunc = D3D11_COMPARISON_LESS;
	
	description.StencilEnable = true;
	description.StencilReadMask = 0xFF;
	description.StencilWriteMask = 0xFF;
	
	description.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	description.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	description.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	description.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	
	description.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	description.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	description.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	description.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	return description;
}

bool Renderer::Initialize(const RendererSetup setup)
{
	IDXGIFactory* factory = nullptr;

	auto result = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)& factory);

	FailFast(result);

	IDXGIAdapter* adapter = nullptr;

	result = factory->EnumAdapters(0, &adapter);

	FailFast(result);

	IDXGIOutput* adapterOutput = nullptr;

	result = adapter->EnumOutputs(0, &adapterOutput);

	FailFast(result);

	UINT modeCount = 0;

	DXGI_MODE_DESC* displayModeList = nullptr;

	result = adapterOutput->GetDisplayModeList(
		DXGI_FORMAT_R8G8B8A8_UNORM,
		DXGI_ENUM_MODES_INTERLACED,
		&modeCount,
		displayModeList);

	FailFast(result);

	displayModeList = new DXGI_MODE_DESC[modeCount];

	UINT decoyModeCount = modeCount;

	result = adapterOutput->GetDisplayModeList(
		DXGI_FORMAT_R8G8B8A8_UNORM,
		DXGI_ENUM_MODES_INTERLACED,
		&decoyModeCount,
		displayModeList);

	FailFast(result);

	DXGI_MODE_DESC displayMode;
	auto refreshRate = 0.0f;

	ZeroMemory(&displayMode, sizeof(displayMode));

	for (UINT i = 0; i < modeCount; i++)
	{
		auto newDisplayMode = displayModeList[i];

		if (newDisplayMode.Height == setup.ScreenWidth &&
			newDisplayMode.Width == setup.ScreenWidth)
		{
			auto newRefreshRate = (float)newDisplayMode.RefreshRate.Numerator / (float)newDisplayMode.RefreshRate.Denominator;

			if (newRefreshRate > refreshRate)
			{
				refreshRate = newRefreshRate;
				displayMode = newDisplayMode;
			}
		}
	}

	SafeDeleteArray(displayModeList);

	SafeRelease(adapterOutput);

	SafeRelease(adapter);

	SafeRelease(factory);

	DXGI_SWAP_CHAIN_DESC swapChainDescription;

	ZeroMemory(&swapChainDescription, sizeof(swapChainDescription));

	swapChainDescription.BufferCount = 1;
	// Setting backbuffer dimensions		
	swapChainDescription.BufferDesc.Width = setup.ScreenWidth;
	swapChainDescription.BufferDesc.Height = setup.ScreenHeight;
	swapChainDescription.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	// Assuming vSync is enabled
	swapChainDescription.BufferDesc.RefreshRate.Numerator = displayMode.RefreshRate.Numerator;
	swapChainDescription.BufferDesc.RefreshRate.Denominator = displayMode.RefreshRate.Denominator;
	swapChainDescription.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDescription.OutputWindow = setup.WindowHandle;
	// Multisampling
	swapChainDescription.SampleDesc.Count = 1;
	swapChainDescription.SampleDesc.Quality = 0;
	swapChainDescription.Windowed = setup.IsWindowed;
	swapChainDescription.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapChainDescription.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	// Should use FLIP_DISCARD for better performance
	swapChainDescription.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	swapChainDescription.Flags = 0;

	D3D_FEATURE_LEVEL featureLevel[] =
	{
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0
	};

	result = D3D11CreateDeviceAndSwapChain(
		nullptr,
		D3D_DRIVER_TYPE_HARDWARE,
		nullptr,
		D3D11_CREATE_DEVICE_DEBUG,
		featureLevel,
		ARRAYSIZE(featureLevel),
		D3D11_SDK_VERSION,
		&swapChainDescription,
		&_swapChain,
		&_device,
		nullptr,
		&_deviceContext);

	FailFast(result);

	ID3D11Debug* debug = nullptr;

	if (SUCCEEDED(_device->QueryInterface(__uuidof(ID3D11Debug), (void**)& debug)))
	{
		ID3D11InfoQueue* info = nullptr;

		if (SUCCEEDED(_device->QueryInterface(__uuidof(ID3D11InfoQueue), (void**) &info)))
		{
			info->SetBreakOnSeverity(D3D11_MESSAGE_SEVERITY_CORRUPTION, true);
			info->SetBreakOnSeverity(D3D11_MESSAGE_SEVERITY_ERROR, true);

			info->Release();
		}

		debug->Release();
	}

	ID3D11Texture2D* backBufferPtr = nullptr;

	result = _swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)(&backBufferPtr));

	FailFast(result);

	result = _device->CreateRenderTargetView(backBufferPtr, nullptr, &_renderTargetView);

	FailFast(result);

	SafeRelease(backBufferPtr);

	D3D11_TEXTURE2D_DESC depthBufferDescription;

	ZeroMemory(&depthBufferDescription, sizeof(depthBufferDescription));

	depthBufferDescription.Width = setup.ScreenWidth;
	depthBufferDescription.Height = setup.ScreenHeight;
	depthBufferDescription.MipLevels = 1;
	depthBufferDescription.ArraySize = 1;
	depthBufferDescription.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthBufferDescription.SampleDesc.Count = 1;
	depthBufferDescription.SampleDesc.Quality = 0;
	depthBufferDescription.Usage = D3D11_USAGE_DEFAULT;
	depthBufferDescription.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthBufferDescription.CPUAccessFlags = 0;
	depthBufferDescription.MiscFlags = 0;

	result = _device->CreateTexture2D(&depthBufferDescription, nullptr, &_depthStencilBuffer);

	FailFast(result);

	 // 3D depth stencil state

	auto depthStencilDescription3D = GetDefaultDepthStencilDescription();

	result = _device->CreateDepthStencilState(&depthStencilDescription3D, &_3dDepthStencilState);

	FailFast(result);

	_deviceContext->OMSetDepthStencilState(_3dDepthStencilState, 1);

	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDescription;

	ZeroMemory(&depthStencilViewDescription, sizeof(depthStencilViewDescription));

	depthStencilViewDescription.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilViewDescription.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDescription.Texture2D.MipSlice = 0;

	result = _device->CreateDepthStencilView(
		_depthStencilBuffer,
		&depthStencilViewDescription,
		&_depthStencilView);

	FailFast(result);

	_deviceContext->OMSetRenderTargets(1, &_renderTargetView, _depthStencilView);

	D3D11_RASTERIZER_DESC rasterizerDescription;

	ZeroMemory(&rasterizerDescription, sizeof(rasterizerDescription));

	rasterizerDescription.AntialiasedLineEnable = false;
	rasterizerDescription.CullMode = D3D11_CULL_BACK;
	rasterizerDescription.DepthBias = 0;
	rasterizerDescription.DepthBiasClamp = 0.0f;
	rasterizerDescription.DepthClipEnable = true;
	rasterizerDescription.FillMode = D3D11_FILL_SOLID;
	rasterizerDescription.FrontCounterClockwise = false;
	rasterizerDescription.MultisampleEnable = false;
	rasterizerDescription.ScissorEnable = false;
	rasterizerDescription.SlopeScaledDepthBias = 0.0f;

	result = _device->CreateRasterizerState(&rasterizerDescription, &_rasterizerState);

	FailFast(result);

	_deviceContext->RSSetState(_rasterizerState);

	D3D11_VIEWPORT viewport;

	ZeroMemory(&viewport, sizeof(viewport));

	viewport.Width = (float)setup.ScreenWidth;
	viewport.Height = (float)setup.ScreenHeight;
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;

	_deviceContext->RSSetViewports(1, &viewport);

	auto fieldOfView = DirectX::XM_PI / 4.0f;
	auto aspectRatio = (float)setup.ScreenWidth / (float)setup.ScreenHeight;

	auto nearPlane = 0.01f;
	auto farPlane = 1000.0f;

	// Hard-coded near and far plane
	_projectionMatrix = DirectX::XMMatrixPerspectiveFovLH(fieldOfView, aspectRatio, nearPlane, farPlane);
	_worldMatrix = DirectX::XMMatrixIdentity();
	_orthoMatrix = DirectX::XMMatrixOrthographicLH((float)setup.ScreenWidth, (float)setup.ScreenHeight, nearPlane, farPlane);
	
	// 2D depth stencil state

	auto depthStencilDescription2D = GetDefaultDepthStencilDescription();

	depthStencilDescription2D.DepthEnable = false;

	result = _device->CreateDepthStencilState(&depthStencilDescription2D, &_2dDepthStencilState);

	FailFast(result);

	_camera = new Camera();
	_camera->SetPosition({ 0, 0, -10 });
	_camera->Update();

	// Texture shader
	_textureShader = new TextureShader();
	_textureShader->Initialize(_device);

	_texture = new Texture2D();
	_texture->Initialize(_device, L"E:\\source\\willborgium\\flex\\source\\bin\\Debug\\test.dds");

	// Sprite 1
	_sprite1 = new Sprite();

	_sprite1->Initialize(_device, _texture, 100, 100);

	/*

	// Colored triangle

	auto vertices = new ColoredVertex[3];

	DirectX::XMFLOAT3 position1(-5, 0, 0);

	vertices[0].Position = DirectX::XMFLOAT3(position1.x - 1, position1.y - 1, 0);
	vertices[0].Color = DirectX::XMFLOAT4(1, 0, 0, 1);

	vertices[1].Position = DirectX::XMFLOAT3(position1.x, position1.y + 1, 0);
	vertices[1].Color = DirectX::XMFLOAT4(0, 1, 0, 1);

	vertices[2].Position = DirectX::XMFLOAT3(position1.x + 1, position1.y - 1, 0);
	vertices[2].Color = DirectX::XMFLOAT4(0, 0, 1, 1);

	auto indices = new unsigned long[3] { 0, 1, 2 };

	_primitive1 = new IndexedPrimitive<ColoredVertex>();
	_primitive1->Initialize(_device, 3, vertices, 3, indices);

	// Textured triangle

	DirectX::XMFLOAT3 position2(0, 0, 0);

	auto vertices2 = new TexturedVertex[3];

	vertices2[0].Position = DirectX::XMFLOAT3(position2.x - 1, position2.y - 1, 0);
	vertices2[0].TextureCoordinate = DirectX::XMFLOAT2(0, 1);

	vertices2[1].Position = DirectX::XMFLOAT3(position2.x, position2.y + 1, 0);
	vertices2[1].TextureCoordinate = DirectX::XMFLOAT2(.5, 0);

	vertices2[2].Position = DirectX::XMFLOAT3(position2.x + 1, position2.y - 1, 0);
	vertices2[2].TextureCoordinate = DirectX::XMFLOAT2(1, 1);

	auto indices2 = new unsigned long[3] { 0, 1, 2 };

	_primitive2 = new IndexedPrimitive<TexturedVertex>();
	_primitive2->Initialize(_device, 3, vertices2, 3, indices2);

	// Textured Normaled triangle

	DirectX::XMFLOAT3 position3(5, 0, 0);

	auto vertices3 = new TexturedNormalVertex[3];

	vertices3[0].Position = DirectX::XMFLOAT3(position3.x - 1, position3.y - 1, 0);
	vertices3[0].TextureCoordinate = DirectX::XMFLOAT2(0, 1);
	vertices3[0].Normal = DirectX::XMFLOAT3(0, 0, -1);

	vertices3[1].Position = DirectX::XMFLOAT3(position3.x, position3.y + 1, 0);
	vertices3[1].TextureCoordinate = DirectX::XMFLOAT2(.5, 0);
	vertices3[1].Normal = DirectX::XMFLOAT3(0, 0, -1);

	vertices3[2].Position = DirectX::XMFLOAT3(position3.x + 1, position3.y - 1, 0);
	vertices3[2].TextureCoordinate = DirectX::XMFLOAT2(1, 1);
	vertices3[2].Normal = DirectX::XMFLOAT3(0, 0, -1);

	auto indices3 = new unsigned long[3]{ 0, 1, 2 };

	_primitive3 = new IndexedPrimitive<TexturedNormalVertex>();
	_primitive3->Initialize(_device, 3, vertices3, 3, indices3);
	
	// Color shader

	_colorShader = new ColorShader();
	_colorShader->Initialize(_device);

	_colorShader->SetMatrices(
		_deviceContext,
		_camera->GetView(),
		_worldMatrix,
		_projectionMatrix);

	 // Diffuse shader
	_diffuseShader = new DiffuseShader();
	_diffuseShader->Initialize(_device);

	_diffuseShader->SetMatrices(
		_deviceContext,
		_camera->GetView(),
		_worldMatrix,
		_projectionMatrix);

	LightInfo light;

	light.Color = DirectX::XMFLOAT4(1.0f, 0.0f, 1.0f, 1.0f);
	light.Direction = DirectX::XMFLOAT3(0, 0, 1);

	_diffuseShader->SetLightInfo(light);
	*/

	return true;
}

void Renderer::Uninitialize()
{
	if (_diffuseShader != nullptr)
	{
		_diffuseShader->Uninitialize();
		SafeDelete(_diffuseShader);
	}

	if (_colorShader != nullptr)
	{
		_colorShader->Uninitialize();
		SafeDelete(_colorShader);
	}
	if (_textureShader != nullptr)
	{
		_textureShader->Uninitialize();
		SafeDelete(_textureShader);
	}

	if (_texture != nullptr)
	{
		_texture->Uninitialize();
		SafeDelete(_texture);
	}

	if (_primitive1 != nullptr)
	{
		_primitive1->Uninitialize();
		SafeDelete(_primitive1);
	}

	if (_primitive2 != nullptr)
	{
		_primitive2->Uninitialize();
		SafeDelete(_primitive2);
	}

	SafeDelete(_camera);

	if (_swapChain != nullptr)
	{
		_swapChain->SetFullscreenState(false, nullptr);
	}

	SafeRelease(_rasterizerState);
	SafeRelease(_depthStencilView);
	SafeRelease(_2dDepthStencilState);
	SafeRelease(_3dDepthStencilState);
	SafeRelease(_depthStencilBuffer);
	SafeRelease(_renderTargetView);
	SafeRelease(_deviceContext);
	SafeRelease(_device);
	SafeRelease(_swapChain);
}

void Renderer::BeginScene(float red, float green, float blue, float alpha)
{
	float color[4];

	color[0] = red;
	color[1] = green;
	color[2] = blue;
	color[3] = alpha;

	_deviceContext->ClearRenderTargetView(_renderTargetView, color);

	_deviceContext->ClearDepthStencilView(_depthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
}

void Renderer::Render()
{
	//_deviceContext->OMSetDepthStencilState(_3dDepthStencilState, 1);

	//_textureShader->SetMatrices(
	//	_deviceContext,
	//	_worldMatrix,
	//	_camera->GetView(),
	//	_projectionMatrix);

	//_textureShader->Apply(_deviceContext);

	//_textureShader->SetTexture(_deviceContext, _texture->GetTexture());

	//_primitive2->Render(_deviceContext);

	/*_colorShader->Apply(_deviceContext);

	_primitive1->Render(_deviceContext);

	_diffuseShader->Apply(_deviceContext);

	_diffuseShader->SetTexture(_deviceContext, _texture->GetTexture());

	_primitive3->Render(_deviceContext);*/

	_deviceContext->OMSetDepthStencilState(_2dDepthStencilState, 1);

	_camera->Update();

	_textureShader->SetMatrices(
		_deviceContext,
		_worldMatrix,
		_camera->GetView(),
		_orthoMatrix);

	_textureShader->Apply(_deviceContext);

	_textureShader->SetTexture(_deviceContext, _sprite1->GetTexture()->GetTexture());

	_sprite1->Update(_deviceContext);

	_sprite1->Render(_deviceContext);
}

void Renderer::EndScene()
{
	_swapChain->Present(1, 0);
}