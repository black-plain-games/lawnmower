#pragma once

#include <d3d11.h>
#include <DirectXMath.h>

#include "ShaderBase.h"

namespace Flex
{
	struct ColoredVertex
	{
		DirectX::XMFLOAT3 Position;
		DirectX::XMFLOAT4 Color;
	};

	class ColorShader : public ShaderBase
	{
	public:
		virtual void Initialize(ID3D11Device*);

	protected:
		virtual D3D11_INPUT_ELEMENT_DESC* GetPolygonLayout(_Out_ unsigned int*);
	};
}