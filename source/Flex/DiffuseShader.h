#pragma once

#include <d3d11.h>
#include <DirectXMath.h>

#include "Sized.h"
#include "ShaderBase.h"

namespace Flex
{
	struct LightInfo
	{
		DirectX::XMFLOAT4 Color;
		DirectX::XMFLOAT3 Direction;
	};

	class DiffuseShader : public ShaderBase
	{
	public:
		DiffuseShader();
		virtual void Initialize(ID3D11Device*);
		virtual void Uninitialize();

		virtual void Apply(ID3D11DeviceContext*);

		inline void SetLightInfo(LightInfo& info) { _data.d = info; }
		void SetTexture(ID3D11DeviceContext*, ID3D11ShaderResourceView*);

	protected:
		virtual D3D11_INPUT_ELEMENT_DESC* GetPolygonLayout(_Out_ unsigned int*);

	private:
		ID3D11Buffer* _lightBuffer;
		Sized<LightInfo, 32> _data;
	};
}