#include "ShaderBase.h"
#include "Utilities.h"

using namespace Flex;

ShaderBase::ShaderBase()
	: ShaderBase(false)
{
}

ShaderBase::ShaderBase(bool usesSamplerState)
	: UsesSamplerState(usesSamplerState)
{
	_matrixBuffer = nullptr;
	_vertexShader = nullptr;
	_pixelShader = nullptr;
	_layout = nullptr;
	_samplerState = nullptr;
	_matrixData = MatrixBuffer();
}

ShaderBase::~ShaderBase()
{
}

void ShaderBase::Initialize(ID3D11Device* device)
{
	_matrixBuffer = CreateConstantBuffer(sizeof(MatrixBuffer), device);
}

void ShaderBase::Uninitialize()
{
	SafeRelease(_matrixBuffer);
	SafeRelease(_layout);
	SafeRelease(_pixelShader);
	SafeRelease(_vertexShader);
	SafeRelease(_samplerState);
}

void ShaderBase::SetMatrices(ID3D11DeviceContext* deviceContext, DirectX::XMMATRIX world, DirectX::XMMATRIX view, DirectX::XMMATRIX projection)
{
	_matrixData.World = DirectX::XMMatrixTranspose(world);
	_matrixData.View = DirectX::XMMatrixTranspose(view);
	_matrixData.Projection = DirectX::XMMatrixTranspose(projection);
}

void ShaderBase::UpdateConstantBuffer(ID3D11DeviceContext* deviceContext, ID3D11Buffer* buffer, void* data, size_t size)
{
	D3D11_MAPPED_SUBRESOURCE mapping;
	auto result = deviceContext->Map(buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapping);

	if (FAILED(result))
	{
		throw std::exception("Failed to map constant buffer");
	}

	memcpy_s(mapping.pData, size, data, size);

	deviceContext->Unmap(buffer, 0);
}

void ShaderBase::Apply(ID3D11DeviceContext* deviceContext)
{
	UpdateConstantBuffer(deviceContext, _matrixBuffer, &_matrixData, sizeof(_matrixData));

	deviceContext->VSSetConstantBuffers(0, 1, &_matrixBuffer);

	deviceContext->IASetInputLayout(_layout);

	deviceContext->VSSetShader(_vertexShader, nullptr, 0);
	deviceContext->PSSetShader(_pixelShader, nullptr, 0);

	if (UsesSamplerState && _samplerState != nullptr)
	{
		deviceContext->PSSetSamplers(0, 1, &_samplerState);
	}
}

void ShaderBase::LoadVertexShader(const char* compiledShaderFilePath, ID3D11Device* device)
{
	size_t length = 0;

	auto buffer = Utilities::LoadFile(compiledShaderFilePath, &length);

	auto result = device->CreateVertexShader(
		(void*)buffer,
		length,
		nullptr,
		&_vertexShader);

	if (FAILED(result))
	{
		throw std::exception("Failed to create vertex shader");
	}

	unsigned int elementCount = 0;

	auto polygonLayout = GetPolygonLayout(&elementCount);

	result = device->CreateInputLayout(
		polygonLayout,
		elementCount,
		buffer,
		length,
		&_layout);

	if (FAILED(result))
	{
		throw std::exception("Failed to create input layout");
	}

	SafeDeleteArray(buffer);

	if (UsesSamplerState)
	{
		auto samplerDescription = GetSamplerDescription();

		result = device->CreateSamplerState(&samplerDescription, &_samplerState);

		if (FAILED(result))
		{
			throw std::exception("Failed to create sampler state");
		}
	}
}

void ShaderBase::LoadPixelShader(const char* compiledShaderFilePath, ID3D11Device* device)
{
	size_t length = 0;

	auto buffer = Utilities::LoadFile(compiledShaderFilePath, &length);

	auto result = device->CreatePixelShader(
		(void*)buffer,
		length,
		nullptr,
		&_pixelShader);

	if (FAILED(result))
	{
		throw std::exception("Failed to create pixel shader");
	}
}

D3D11_SAMPLER_DESC ShaderBase::GetSamplerDescription()
{
	// This is a default texture sampler description, subclasses
	// should override and redefine this if necessary
	auto output = D3D11_SAMPLER_DESC();
	ZeroMemory(&output, sizeof(D3D11_SAMPLER_DESC));

	output.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	output.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	output.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	output.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	output.MipLODBias = 0.0f;
	output.MaxAnisotropy = 1;
	output.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	output.BorderColor[0] = 0;
	output.BorderColor[1] = 0;
	output.BorderColor[2] = 0;
	output.BorderColor[3] = 0;
	output.MinLOD = 0;
	output.MaxLOD = D3D11_FLOAT32_MAX;

	return output;
}

void ShaderBase::InitializeLayoutElement(D3D11_INPUT_ELEMENT_DESC* element)
{
	element->SemanticName = nullptr;
	element->SemanticIndex = 0;
	element->Format = DXGI_FORMAT_UNKNOWN;
	element->InputSlot = 0;
	element->AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	element->InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	element->InstanceDataStepRate = 0;
}

void ShaderBase::InitializeLayoutElements(D3D11_INPUT_ELEMENT_DESC* elements, unsigned int elementCount)
{
	for (unsigned int index = 0; index < elementCount; index++)
	{
		InitializeLayoutElement(&elements[index]);
	}
}

void ShaderBase::InitializePositionLayoutElement(D3D11_INPUT_ELEMENT_DESC* element)
{
	InitializeLayoutElement(element);

	element->SemanticName = "POSITION";
	element->Format = DXGI_FORMAT_R32G32B32_FLOAT;
}

void ShaderBase::InitializeTextureLayoutElement(D3D11_INPUT_ELEMENT_DESC* element)
{
	InitializeLayoutElement(element);

	element->SemanticName = "TEXCOORD";
	element->Format = DXGI_FORMAT_R32G32_FLOAT;
}

D3D11_BUFFER_DESC ShaderBase::GetConstantBufferDescription(unsigned int byteWidth)
{
	auto output = D3D11_BUFFER_DESC();
	ZeroMemory(&output, sizeof(D3D11_BUFFER_DESC));

	output.Usage = D3D11_USAGE_DYNAMIC;
	output.ByteWidth = byteWidth;
	output.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	output.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	output.MiscFlags = 0;
	output.StructureByteStride = 0;

	return output;
}

ID3D11Buffer* ShaderBase::CreateConstantBuffer(unsigned int byteWidth, ID3D11Device* device)
{
	auto description = GetConstantBufferDescription(sizeof(MatrixBuffer));

	ID3D11Buffer* output = nullptr;

	auto result = device->CreateBuffer(&description, nullptr, &output);

	if (FAILED(result))
	{
		throw std::exception("Failed to create constant buffer");
	}

	return output;
}
