#pragma once

#include <DirectXMath.h>

#include "Align.h"

namespace Flex
{
	class Camera : public Align<16>
	{
	public:
		Camera();
		~Camera();

		inline void SetPosition(DirectX::XMFLOAT3 position) { _position = position; }
		inline DirectX::XMFLOAT3 GetPosition() { return _position; }

		inline void SetRotation(DirectX::XMFLOAT3 rotation) { _rotation = rotation; }
		inline DirectX::XMFLOAT3 GetRotation() { return _rotation; }

		inline DirectX::XMMATRIX GetView() { return _view; }

		void Update();

	private:
		DirectX::XMFLOAT3 _position;
		DirectX::XMFLOAT3 _rotation;
		DirectX::XMMATRIX _view;
	};
}