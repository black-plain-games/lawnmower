#pragma once

namespace Flex
{
	template <typename DataType, unsigned int Size>
	union Sized
	{
		DataType d;
		unsigned char _[Size];
	};
}