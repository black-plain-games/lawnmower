#include <exception>

#include "ColorShader.h"
#include "Utilities.h"

using namespace Flex;

void ColorShader::Initialize(ID3D11Device* device)
{
	LoadVertexShader("ColorVertexShader.cso", device);

	LoadPixelShader("ColorPixelShader.cso", device);

	ShaderBase::Initialize(device);
}

D3D11_INPUT_ELEMENT_DESC* ColorShader::GetPolygonLayout(_Out_ unsigned int* elementCount)
{
	auto output = new D3D11_INPUT_ELEMENT_DESC[2];

	InitializePositionLayoutElement(&output[0]);

	InitializeLayoutElement(&output[1]);

	output[1].SemanticName = "COLOR";
	output[1].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;

	(*elementCount) = 2;

	return output;
}