#pragma once

#include <d3d11.h>
#include <DirectXMath.h>

#include "Texture2D.h"

class Model
{
public:
	Model();

	void Initialize(ID3D11Device*);

	void Uninitialize();

	void Render(ID3D11DeviceContext*);

	inline void SetTexture(Texture2D* texture) { _texture = texture; }

	inline Texture2D* GetTexture() { return _texture; }

private:
	ID3D11Buffer* _vertexBuffer;
	ID3D11Buffer* _indexBuffer;
	Texture2D* _texture;
	int _vertexCount;
	int _indexCount;

	struct Vertex
	{
		DirectX::XMFLOAT3 Position;
		DirectX::XMFLOAT4 Color;
		//DirectX::XMFLOAT2 TextureCoordinate;
	};
};