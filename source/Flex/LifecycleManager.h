#pragma once

namespace Flex
{
	class LifecycleManager
	{
	public:
		virtual void PreInitialize() { };
		virtual void PostInitialize() { };
		virtual void PreUpdate() { };
		virtual void PostUpdate() { };
		virtual void PreRender() { };
		virtual void PostRender() { };
		virtual void PreUnititialize() { };
		virtual int PostUninitialize() { return 0; };
	};
}