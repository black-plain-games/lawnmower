Texture2D Texture;

SamplerState SampleType;

struct PixelInputType
{
	float4 Position : SV_POSITION;
	float2 TextureCoordinate: TEXCOORD0;
};

float4 main(PixelInputType input) : SV_TARGET
{
	return Texture.Sample(SampleType, input.TextureCoordinate);
}