#pragma once

#include <d3d11.h>

namespace Flex
{
	class PrimitiveBase
	{
	public:
		PrimitiveBase();

		void Initialize(ID3D11Device*, unsigned long, unsigned int, void*, unsigned long, unsigned long*);

		void Uninitialize();

		void Render(ID3D11DeviceContext*);

	private:
		ID3D11Buffer* _vertexBuffer;
		ID3D11Buffer* _indexBuffer;

		unsigned int _vertexStride;
		unsigned long _vertexCount;
		unsigned long _indexCount;
	};
}