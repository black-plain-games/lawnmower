#include "IndexedPrimitive.h"

#include "Utilities.h"

template<typename TVertex>
IndexedPrimitive<TVertex>::IndexedPrimitive()
{
	_vertexBuffer = nullptr;
	_indexBuffer = nullptr;
}

template<typename TVertex>
void IndexedPrimitive<TVertex>::Initialize(
	ID3D11Device* device,
	unsigned long vertexCount,
	TVertex* vertices,
	unsigned long indexCount,
	unsigned long* indices)
{
}
