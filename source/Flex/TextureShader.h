#pragma once

#include <d3d11.h>

#include "ShaderBase.h"

namespace Flex
{
	class TextureShader : public ShaderBase
	{
	public:
		TextureShader() : ShaderBase(true) { }
		virtual void Initialize(ID3D11Device*);

		void SetTexture(ID3D11DeviceContext*, ID3D11ShaderResourceView*);

	protected:
		virtual D3D11_INPUT_ELEMENT_DESC* GetPolygonLayout(_Out_ unsigned int*);
	};
}