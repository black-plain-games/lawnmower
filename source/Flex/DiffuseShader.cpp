#include "DiffuseShader.h"
#include "Utilities.h"

using namespace Flex;

DiffuseShader::DiffuseShader()
	: ShaderBase(true)
{
	_lightBuffer = nullptr;
	_data = Sized<LightInfo, 32>();
}

void DiffuseShader::Initialize(ID3D11Device* device)
{
	LoadVertexShader("DiffuseVertexShader.cso", device);

	LoadPixelShader("DiffusePixelShader.cso", device);

	_lightBuffer = CreateConstantBuffer(sizeof(_data), device);

	ShaderBase::Initialize(device);
}

void DiffuseShader::Uninitialize()
{
	SafeRelease(_lightBuffer);

	ShaderBase::Uninitialize();
}

void DiffuseShader::Apply(ID3D11DeviceContext* deviceContext)
{
	UpdateConstantBuffer(deviceContext, _lightBuffer, &_data.d, sizeof(_data.d));

	deviceContext->PSSetConstantBuffers(0, 1, &_lightBuffer);

	ShaderBase::Apply(deviceContext);
}

void DiffuseShader::SetTexture(ID3D11DeviceContext* deviceContext, ID3D11ShaderResourceView* texture)
{
	deviceContext->PSSetShaderResources(0, 1, &texture);
}

D3D11_INPUT_ELEMENT_DESC* DiffuseShader::GetPolygonLayout(_Out_ unsigned int* elementCount)
{
	auto output = new D3D11_INPUT_ELEMENT_DESC[3];

	InitializePositionLayoutElement(&output[0]);

	InitializeTextureLayoutElement(&output[1]);

	InitializeLayoutElement(&output[2]);

	output[2].SemanticName = "NORMAL";
	output[2].Format = DXGI_FORMAT_R32G32B32_FLOAT;

	(*elementCount) = 3;

	return output;
}
