#include "TextureShader.h"

using namespace Flex;

void TextureShader::Initialize(ID3D11Device* device)
{
	LoadVertexShader("E:\\source\\willborgium\\flex\\source\\bin\\Debug\\TextureVertexShader.cso", device);

	LoadPixelShader("E:\\source\\willborgium\\flex\\source\\bin\\Debug\\TexturePixelShader.cso", device);

	ShaderBase::Initialize(device);
}

void TextureShader::SetTexture(ID3D11DeviceContext* deviceContext, ID3D11ShaderResourceView* texture)
{
	deviceContext->PSSetShaderResources(0, 1, &texture);
}

D3D11_INPUT_ELEMENT_DESC* TextureShader::GetPolygonLayout(_Out_ unsigned int* elementCount)
{
	auto output = new D3D11_INPUT_ELEMENT_DESC[2];

	InitializePositionLayoutElement(&output[0]);

	InitializeTextureLayoutElement(&output[1]);

	(*elementCount) = 2;

	return output;
}