#include "Texture2D.h"

#include <exception>

#include "DDSTextureLoader.h"

#include "Utilities.h"

using namespace Flex;

Texture2D::Texture2D()
{
	_texture = nullptr;
}

Texture2D::~Texture2D()
{
}

void Texture2D::Initialize(ID3D11Device* device, const WCHAR* fileName)
{
	auto result = DirectX::CreateDDSTextureFromFile(device, fileName, nullptr, &_texture);

	if (FAILED(result))
	{
		throw std::exception("Failed to initialize a texture");
	}
}

void Texture2D::Uninitialize()
{
	SafeRelease(_texture);
}
