#pragma once

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <Windows.h>

#include "LifecycleManager.h"

namespace Flex
{
	struct EngineSetup
	{
		const char* WindowCaption;
		const char* WindowName;
		unsigned int ScreenWidth;
		unsigned int ScreenHeight;
		bool IsWindowed;
	};

	class Engine
	{
	public:
		Engine(LifecycleManager*);

		int Run(HINSTANCE instanceHandle, EngineSetup setup);

	private:
		LifecycleManager* _lifecycleManager;
	};
}