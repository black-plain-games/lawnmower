Texture2D Texture;
SamplerState SampleType;

cbuffer LightBuffer
{
	float4 Color;
	float3 Direction;
	float Padding;
};

struct PixelInput
{
	float4 Position : SV_POSITION;
	float2 TextureCoordinates : TEXCOORD0;
	float3 Normal : NORMAL;
};

float4 main(PixelInput input) : SV_TARGET
{
	float4 textureColor = Texture.Sample(SampleType, input.TextureCoordinates);

	float intensity = saturate(dot(input.Normal, -Direction));

	float4 color = saturate(Color * intensity);

	return color * textureColor;
}