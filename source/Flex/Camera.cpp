#include "Camera.h"

using namespace Flex;

using namespace DirectX;

Camera::Camera()
{
	_position = { 0, 0, 0 };
	_rotation = { 0, 0, 0 };
	_view = XMMatrixIdentity();
}

Camera::~Camera()
{
}

void Camera::Update()
{
	XMFLOAT3 up = { 0, 1, 0 };
	XMFLOAT3 position = _position;
	XMFLOAT3 target = { 0, 0, 1 };

	auto pitch = XMConvertToRadians(_rotation.x);
	auto yaw = XMConvertToRadians(_rotation.y);
	auto roll = XMConvertToRadians(_rotation.z);

	auto rotation = XMMatrixRotationRollPitchYaw(pitch, yaw, roll);

	auto positionVector = XMLoadFloat3(&position);
	auto targetVector = XMVector3TransformCoord(XMLoadFloat3(&target), rotation);
	auto upVector = XMVector3TransformCoord(XMLoadFloat3(&up), rotation);

	targetVector += positionVector;

	_view = XMMatrixLookAtLH(positionVector, targetVector, upVector);
}