#pragma once

#include <filesystem>
#include <fstream>

// requires C++17 or above
namespace fs = std::filesystem;

namespace Flex
{
#ifndef SafeDelete

#define SafeDelete(x) if (x != nullptr) { delete x; x = nullptr; }

#endif

#ifndef SafeDeleteArray

#define SafeDeleteArray(x) if (x != nullptr) { delete[] x; x = nullptr; }

#endif

#ifndef SafeRelease

#define SafeRelease(x) if(x != nullptr) { x->Release(); x = nullptr; }

#endif

#ifndef FailFast

#define FailFast(x) if (FAILED(x)) return false;

#endif

	namespace Utilities
	{
		inline char* LoadFile(const char* fileName, size_t* fileLength)
		{
			std::ifstream file;

			file.open(fileName, std::ios::in | std::ios::binary);

			auto length = (unsigned int)fs::file_size(fileName);

			file.seekg(0, std::ios::beg);

			auto buffer = new char[length];

			file.read(buffer, length);
			file.close();

			(*fileLength) = length;

			return buffer;
		}

		inline char* Concatenate(const char* a, const char* b)
		{
			auto aLength = strlen(a);
			auto bLength = strlen(b);
			auto length = aLength + bLength;
			auto output = new char[length];
			memset(output, 0, length);
			strcpy_s(output, aLength, a);
			strcat_s(output, bLength, b);
			return output;
		}
	}
}