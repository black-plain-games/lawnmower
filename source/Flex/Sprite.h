#pragma once

#include <d3d11.h>
#include "TexturedVertex.h"

#include "Utilities.h"
#include "Texture2D.h"

namespace Flex
{
	class Sprite
	{
	public:
		void Initialize(ID3D11Device* device, Texture2D* texture, float width, float height)
		{
			_texture = texture;

			_width = width;

			_height = height;

			_isDirty = true;

			auto vertices = new TexturedVertex[4];

			D3D11_BUFFER_DESC vertexBufferDescription;

			//ZeroMemory(&vertexBufferDescription, sizeof(D3D11_BUFFER_DESC));

			vertexBufferDescription.Usage = D3D11_USAGE_DYNAMIC;
			vertexBufferDescription.ByteWidth = sizeof(TexturedVertex) * 4;
			vertexBufferDescription.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			vertexBufferDescription.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
			vertexBufferDescription.MiscFlags = 0;
			vertexBufferDescription.StructureByteStride = 0;

			D3D11_SUBRESOURCE_DATA vertexData;

			//ZeroMemory(&vertexData, sizeof(D3D11_SUBRESOURCE_DATA));

			vertexData.pSysMem = vertices;
			vertexData.SysMemPitch = 0;
			vertexData.SysMemSlicePitch = 0;

			auto result = device->CreateBuffer(&vertexBufferDescription, &vertexData, &_vertexBuffer);

			if (FAILED(result))
			{
				throw std::exception("Could not create vertex buffer");
			}

			D3D11_BUFFER_DESC indexBufferDescription;

			//ZeroMemory(&indexBufferDescription, sizeof(D3D11_BUFFER_DESC));

			indexBufferDescription.Usage = D3D11_USAGE_DEFAULT;
			indexBufferDescription.ByteWidth = sizeof(unsigned long) * 6;
			indexBufferDescription.BindFlags = D3D11_BIND_INDEX_BUFFER;
			indexBufferDescription.CPUAccessFlags = 0;
			indexBufferDescription.MiscFlags = 0;
			indexBufferDescription.StructureByteStride = 0;

			D3D11_SUBRESOURCE_DATA indexData;

			//ZeroMemory(&indexData, sizeof(D3D11_SUBRESOURCE_DATA));

			auto indices = new long[6]
			{
				0, 3, 2,
				0, 1, 3
			};

			indexData.pSysMem = indices;
			indexData.SysMemPitch = 0;
			indexData.SysMemSlicePitch = 0;

			result = device->CreateBuffer(&indexBufferDescription, &indexData, &_indexBuffer);

			if (FAILED(result))
			{
				throw std::exception("Could not create vertex buffer");
			}

			SafeDeleteArray(indices);
			SafeDeleteArray(vertices);
		}

		void Update(ID3D11DeviceContext* context)
		{
			if (!_isDirty)
			{
				return;
			}

			D3D11_MAPPED_SUBRESOURCE mappedResource;

			auto result = context->Map(_vertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

			if (FAILED(result))
			{
				throw new std::exception("Failed to map vertex buffer");
			}

			auto vertices = (TexturedVertex*)mappedResource.pData;

			// calculate position
			vertices[0].Position = DirectX::XMFLOAT3(_positionX, _positionY, 0);
			vertices[1].Position = DirectX::XMFLOAT3(_positionX + _width, _positionY, 0);
			vertices[2].Position = DirectX::XMFLOAT3(_positionX, _positionY + _height, 0);
			vertices[3].Position = DirectX::XMFLOAT3(_positionX + _width, _positionY + _height, 0);

			// calculate texture
			vertices[0].TextureCoordinate = DirectX::XMFLOAT2(0, 0);
			vertices[1].TextureCoordinate = DirectX::XMFLOAT2(1, 0);
			vertices[2].TextureCoordinate = DirectX::XMFLOAT2(0, 1);
			vertices[3].TextureCoordinate = DirectX::XMFLOAT2(1, 1);

			context->Unmap(_vertexBuffer, 0);

			_isDirty = false;
		}

		void Render(ID3D11DeviceContext* context)
		{
			unsigned int stride = sizeof(TexturedVertex);
			unsigned int offset = 0;

			context->IASetVertexBuffers(0, 1, &_vertexBuffer, &stride, &offset);

			context->IASetIndexBuffer(_indexBuffer, DXGI_FORMAT_R32_UINT, 0);

			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

			context->DrawIndexed(6, 0, 0);
		}

		void SetPosition(float x, float y)
		{
			if (x != _positionX)
			{
				_positionX = x;
				_isDirty = true;
			}

			if (y != _positionY)
			{
				_positionY = y;
				_isDirty = true;
			}
		}
		
		void Move(float x, float y)
		{
			SetPosition(_positionX + x, _positionY + y);
		}
		
		inline Texture2D* GetTexture() { return _texture; }
	protected:
	private:
		ID3D11Buffer* _vertexBuffer;
		ID3D11Buffer* _indexBuffer;
		Texture2D* _texture;
		float _positionX, _positionY;
		float _width, _height;
		bool _isDirty;
	};
}