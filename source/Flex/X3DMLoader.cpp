#include "X3DMLoader.h"

#include <DirectXMath.h>
#include <exception>
#include <functional>

#include "Utilities.h"

using namespace Flex;

struct X3DVertex
{
	bool IsPositionSet;
	DirectX::XMFLOAT3 Position;
	bool HasTextureCoordinate;
	DirectX::XMFLOAT2 TextureCoordinate;
	bool HasNormal;
	DirectX::XMFLOAT3 Normal;

	X3DVertex()
	{
		IsPositionSet = false;
		Position = DirectX::XMFLOAT3(0, 0, 0);
		HasTextureCoordinate = false;
		TextureCoordinate = DirectX::XMFLOAT2(0, 0);
		HasNormal = false;
		Normal = DirectX::XMFLOAT3(0, 0, 0);
	}
};

struct X3DModel
{
	unsigned int VertexCount;
	unsigned int IndexCount;

	X3DVertex* Vertices;
	unsigned int Indices;

	X3DModel()
	{
		VertexCount = 0;
		IndexCount = 0;
		Vertices = nullptr;
		Indices = 0;
	}
};

template <typename Type>
class Collection
{
public:
	Collection()
	{
		_start = nullptr;
		_end = nullptr;
		_count = 0;
	}

	inline int GetCount() { return _count; }

	void Append(Type value)
	{
		_count++;

		auto element = new Container(value);

		if (_start == nullptr)
		{
			_start = _end = element;

			return;
		}

		element->Previous = _end;
		_end->Next = element;
		_end = element;
	}

	Type ElementAt(unsigned int index)
	{
		if (index > _count / 2)
		{
			auto iterator = _end;
			auto position = _count - 1;

			while (position != index)
			{
				iterator = iterator->Previous;
				position--;
			}

			return iterator->Value;
		}

		auto iterator = _start;
		auto position = 0;

		while (position != index)
		{
			iterator = iterator->Next;
			position++;
		}

		return iterator->Value;
	}

	Type Find(std::function<bool(Type)> predicate)
	{
		auto iterator = _start;

		while (iterator != nullptr)
		{
			if (predicate(iterator->Value))
			{
				return iterator->Value;
			}
		}
	}
private:
	struct Container
	{
		Type Value;
		Container* Next;
		Container* Previous;

		Container(Type value)
		{
			Value = value;
			Next = nullptr;
			Previous = nullptr;
		}
	};

	Container* _start;
	Container* _end;
	unsigned int _count;
};

struct DataSectionValue
{
	char* Data;
	void* Value;

	DataSectionValue(char* data)
	{
		Data = data;
		Value = nullptr;
	}
};

struct DataSection
{
	char Identifier;
	Collection<DataSectionValue*> Data;
	unsigned int Length;

	DataSection(char identifier)
	{
		Identifier = identifier;
		Length = 0;
	}
};

char* GetToken(char** currentIndex, char terminator, unsigned int* tokenLength)
{
	char* tokenStart = *currentIndex;
	char* tokenEnd = tokenStart;

	*tokenLength = 0;

	while (*tokenEnd != 0 && *tokenEnd != terminator)
	{
		tokenLength++;
		tokenEnd++;
	}

	if (tokenLength == 0)
	{
		return nullptr;
	}

	(*tokenLength) += 1;

	auto token = new char[*tokenLength];

	memset(token, 0, *tokenLength);

	memcpy(token, tokenStart, *tokenLength - 1);

	*currentIndex = tokenEnd + 1;

	return token;
}

bool Equals(const char* a, const char* b, unsigned int length = 0)
{
	if (length == 0)
	{
		return strcmp(a, b) == 0;
	}

	return strncmp(a, b, length) == 0;
}

float* GetFloatArray(char* data, unsigned int expectedCount)
{
	auto iterator = data;
	auto tokenLength = 0U;

	auto output = new float[expectedCount];

	for (auto index = 0U; index < expectedCount; index++)
	{
		auto value = GetToken(&iterator, ',', &tokenLength);

		output[index] = atof(value);
	}

	return output;
}

void X3DMLoader::LoadModel(const char* fileName)
{
	auto output = new X3DModel();

	Collection<DataSection*> dataSections;

	size_t length = 0;

	auto fileData = Utilities::LoadFile(fileName, &length);

	auto iterator = fileData;

	unsigned int tokenLength = 0;

	char* token = GetToken(&iterator, ';', &tokenLength);

	while (token != nullptr)
	{
		if (Equals(token, "MET"))
		{
			while (true)
			{
				token = GetToken(&iterator, ';', &tokenLength);

				if (strncmp(token, "VC", 2) == 0)
				{
					output->VertexCount = atoi(token + 2);
				}
				else if (strncmp(token, "IC", 2) == 0)
				{
					output->IndexCount = atoi(token + 2);
				}
				else
				{
					break;
				}
			}
		}
		else if (Equals(token, "VTX"))
		{
			if (output->VertexCount == 0)
			{
				throw std::exception("Cannot create vertex buffer of size 0");
			}

			output->Vertices = new X3DVertex[output->VertexCount];

			auto vertex = output->Vertices;

			token = GetToken(&iterator, ';', &tokenLength);

			while (!Equals(token, "MET") &&
				   !Equals(token, "VTX") &&
				   !Equals(token, "TRI") &&
				   !Equals(token, "DAT", 3))
			{
				auto processedLength = 0U;

				while (processedLength < tokenLength)
				{
					auto subIterator = iterator;
					auto subTokenLength = 0U;

					auto subToken = GetToken(&subIterator, '|', &subTokenLength); // PA0

					processedLength += subTokenLength;

					if (subTokenLength < 3)
					{
						throw std::exception(Utilities::Concatenate("Failed to process vertex component ", subToken));
					}

					auto componentId = subToken[0];
					auto dataId = subToken[1];
					auto dataIndex = atoi(subToken + 2);

					auto dataSection = dataSections.Find([&dataId](DataSection* ds) { return ds->Identifier == dataId; });

					switch (componentId)
					{
					case 'P':
						(*vertex).IsPositionSet = true;
						//(*vertex).Position = DirectX::XMFLOAT3(GetFloatArray();
						break;
					case 'T':
						(*vertex).HasTextureCoordinate = true;
						break;
					case 'N':
						(*vertex).HasNormal = true;
						break;
					}
				}

				token = GetToken(&iterator, ';', &tokenLength);

				vertex++; // possibly a bug
			}
		}
		else if (Equals(token, "TRI"))
		{
		}
		else if (Equals(token, "DAT", 3))
		{
			auto section = new DataSection(token[3]);

			dataSections.Append(section);

			token = GetToken(&iterator, ';', &tokenLength);

			while (!Equals(token, "MET") &&
				   !Equals(token, "VTX") &&
				   !Equals(token, "TRI") &&
				   !Equals(token, "DAT", 3))
			{
				section->Data.Append(new DataSectionValue(token));

				token = GetToken(&iterator, ';', &tokenLength);
			}
		}
	}
}