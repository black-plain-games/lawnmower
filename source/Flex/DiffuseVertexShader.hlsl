cbuffer MatrixBuffer
{
	matrix World;
	matrix View;
	matrix Projection;
};

struct VertexInput
{
	float4 Position : POSITION;
	float2 TextureCoordinates: TEXCOORD0;
	float3 Normal : NORMAL;
};

struct PixelInput
{
	float4 Position : SV_POSITION;
	float2 TextureCoordinates : TEXCOORD0;
	float3 Normal : NORMAL;
};

PixelInput main(VertexInput input)
{
	PixelInput output;

	input.Position.w = 1.0f;

	output.Position = mul(input.Position, World);
	output.Position = mul(output.Position, View);
	output.Position = mul(output.Position, Projection);

	output.TextureCoordinates = input.TextureCoordinates;

	output.Normal = mul(input.Normal, (float3x3)World);
	output.Normal = normalize(output.Normal);

	return output;
}