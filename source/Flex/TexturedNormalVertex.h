#pragma once

#include <DirectXMath.h>

#include "TexturedVertex.h"

namespace Flex
{
	struct TexturedNormalVertex : public TexturedVertex
	{
		DirectX::XMFLOAT3 Normal;
	};
}