#pragma once

#include <d3d11.h>
#include <DirectXMath.h>

namespace Flex
{
	class ShaderBase
	{
	public:
		ShaderBase();
		ShaderBase(bool);
		virtual ~ShaderBase();

		virtual void Initialize(ID3D11Device*);
		virtual void Uninitialize();

		virtual void Apply(ID3D11DeviceContext*);

		void SetMatrices(ID3D11DeviceContext*, DirectX::XMMATRIX, DirectX::XMMATRIX, DirectX::XMMATRIX);

	protected:
		//inline ID3D11Buffer* GetMatrixBuffer() { return _matrixBuffer; }

		void UpdateConstantBuffer(ID3D11DeviceContext*, ID3D11Buffer*, void*, size_t);

		void LoadVertexShader(const char*, ID3D11Device*);
		void LoadPixelShader(const char*, ID3D11Device*);

		virtual D3D11_INPUT_ELEMENT_DESC* GetPolygonLayout(_Out_ unsigned int*) = 0;
		virtual D3D11_SAMPLER_DESC GetSamplerDescription();

		void InitializeLayoutElement(D3D11_INPUT_ELEMENT_DESC*);
		void InitializeLayoutElements(D3D11_INPUT_ELEMENT_DESC*, unsigned int);

		void InitializePositionLayoutElement(D3D11_INPUT_ELEMENT_DESC*);
		void InitializeTextureLayoutElement(D3D11_INPUT_ELEMENT_DESC*);

		D3D11_BUFFER_DESC GetConstantBufferDescription(unsigned int);
		ID3D11Buffer* CreateConstantBuffer(unsigned int, ID3D11Device*);

		const bool UsesSamplerState;

		struct MatrixBuffer
		{
			DirectX::XMMATRIX World;
			DirectX::XMMATRIX View;
			DirectX::XMMATRIX Projection;
		};
		//private:
		MatrixBuffer _matrixData;
		ID3D11Buffer* _matrixBuffer;
		ID3D11VertexShader* _vertexShader;
		ID3D11PixelShader* _pixelShader;
		ID3D11InputLayout* _layout;
		ID3D11SamplerState* _samplerState;
	};
}