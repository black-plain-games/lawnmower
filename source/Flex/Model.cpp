#include <exception>

#include "Model.h"
#include "Utilities.h"

Model::Model()
{
	_vertexCount = 0;
	_indexCount = 0;

	_indexBuffer = nullptr;
	_vertexBuffer = nullptr;

	_texture = nullptr;
}

void Model::Initialize(ID3D11Device* device)
{
	_vertexCount = 3;

	_indexCount = 3;

	auto vertices = new Vertex[_vertexCount];

	vertices[0].Position = DirectX::XMFLOAT3(-1, -1, 0);
	vertices[0].Color = DirectX::XMFLOAT4(1, 0, 0, 1);
	//vertices[0].TextureCoordinate = DirectX::XMFLOAT2(0, 1);

	vertices[1].Position = DirectX::XMFLOAT3(0, 1, 0);
	vertices[1].Color = DirectX::XMFLOAT4(0, 1, 0, 1);
	//vertices[1].TextureCoordinate = DirectX::XMFLOAT2(.5, 0);

	vertices[2].Position = DirectX::XMFLOAT3(1, -1, 0);
	vertices[2].Color = DirectX::XMFLOAT4(0, 0, 1, 1);
	//vertices[2].TextureCoordinate = DirectX::XMFLOAT2(1, 1);

	auto indices = new unsigned long[_indexCount] { 0, 1, 2 };

	D3D11_BUFFER_DESC vertexBufferDescription;
	ZeroMemory(&vertexBufferDescription, sizeof(vertexBufferDescription));

	vertexBufferDescription.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDescription.ByteWidth = sizeof(Vertex) * _vertexCount;
	vertexBufferDescription.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDescription.CPUAccessFlags = 0;
	vertexBufferDescription.MiscFlags = 0;
	vertexBufferDescription.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA vertexData;
	ZeroMemory(&vertexData, sizeof(vertexData));

	vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	auto result = device->CreateBuffer(&vertexBufferDescription, &vertexData, &_vertexBuffer);
	
	if (FAILED(result))
	{
		throw std::exception("Failed to create vertex buffer");
	}

	D3D11_BUFFER_DESC indexBufferDescription;
	ZeroMemory(&indexBufferDescription, sizeof(indexBufferDescription));

	indexBufferDescription.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDescription.ByteWidth = sizeof(unsigned long) * _indexCount;
	indexBufferDescription.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDescription.CPUAccessFlags = 0;
	indexBufferDescription.MiscFlags = 0;
	indexBufferDescription.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA indexData;
	ZeroMemory(&indexData, sizeof(indexData));

	indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	result = device->CreateBuffer(&indexBufferDescription, &indexData, &_indexBuffer);

	if (FAILED(result))
	{
		throw std::exception("Failed to create index buffer");
	}

	SafeDeleteArray(vertices);
	SafeDeleteArray(indices);
}

void Model::Uninitialize()
{
	SafeRelease(_indexBuffer);
	SafeRelease(_vertexBuffer);
}

void Model::Render(ID3D11DeviceContext* deviceContext)
{
	unsigned int stride = sizeof(Vertex);
	unsigned int offset = 0;

	deviceContext->IASetVertexBuffers(0, 1, &_vertexBuffer, &stride, &offset);
	deviceContext->IASetIndexBuffer(_indexBuffer, DXGI_FORMAT_R32_UINT, 0);
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	deviceContext->DrawIndexed(_indexCount, 0, 0);
}
