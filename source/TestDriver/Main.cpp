#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <Windows.h>

#include "..\Flex\Engine.h"
#include "..\Flex\X3DMLoader.h"
#include "..\Flex\LifecycleManager.h"

#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3d11.lib")

#pragma comment(lib, "DirectXTK.lib")

#pragma comment(lib, "Flex.lib")

INT WINAPI WinMain(
	_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPSTR lpCmdLine,
	_In_ INT nShowCmd)
{
	auto lifecycleManager = new Flex::LifecycleManager();

	auto engine = new Flex::Engine(lifecycleManager);

	Flex::EngineSetup setup;

	setup.ScreenWidth = 1920;
	setup.ScreenHeight = 1080;
	setup.IsWindowed = true;
	setup.WindowName = "FlexEngineTest";
	setup.WindowCaption = "Flex Engine Test";

	auto result = engine->Run(hInstance, setup);

	delete engine;

	delete lifecycleManager;

	return result;
}